class CfgPatches
{
    class rof_retextures_un
    {
        units[] = {};
        weapons[] = {};
        requiredVersion = 1.32;
        requiredAddons[] = {
        	"rhsusf_c_weapons",
            "mbg_m16a2"
        };
        name = "Republic of Folk compatibility fixes";
        author = "Republic of Folk";
        url = "";
    };
};

class CfgWeapons
{
    class arifle_MX_Base_F;
    class rhs_weap_m4_Base : arifle_MX_Base_F
    {
        magazines[] =
        {
            // rhs mags
            "rhs_mag_30Rnd_556x45_Mk318_Stanag",
            "rhs_mag_30Rnd_556x45_Mk262_Stanag",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_No_Tracer",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Red",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Green",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Yellow",
            "rhs_mag_30Rnd_556x45_M855A1_Stanag_Tracer_Orange",
            "rhs_mag_30Rnd_556x45_M200_Stanag",

            // bis mags compatibility
            "30Rnd_556x45_Stanag",
            "30Rnd_556x45_Stanag_Tracer_Red",
            "30Rnd_556x45_Stanag_Tracer_Green",
            "30Rnd_556x45_Stanag_Tracer_Yellow",

            // mbg mags
            "mbg_mag_20Rnd_556x45_M855A1_Stanag"
        };
    };
};
